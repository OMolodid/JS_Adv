const eat = function(){
  // console.log(this.name);
  switch (this.name) {
      case "Страус":
          {
              console.log("Ем травку и козявок");
              break;
          };
      case "Голубь":
          {
              console.log("Люблю кушать семечки.");
              break;
          };

      case "Курица":
          {
              console.log("Я неприхотлива в еде. Зернышки вполне устроят.");
              break;
          };

      case "Пингвин":
          {
              console.log("Люблю кушать рыбу.");
              break;
          };
      case "Чайка":
              {
              console.log("Кушаю рыбку и суши");
              break;
              };
      case "Ястреб":
              {
              console.log("На ужин предпочитаю мышей и всякую мелочь!");
              break;
              };
      default:
          {
              console.log("На ужин предпочитаю мышей и всякую мелочь!");

          };
        }
}
  export default eat;
