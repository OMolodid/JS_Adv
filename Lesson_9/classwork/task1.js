/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import makeEggs from './makeEggs';
import fly from './fly';
import swimm from './swimm';
import eat from './eat';
import hunt from './hunt';
import sing from './sing';
import post from './post';
import run from './run';


 function Bird(name) {
     this.name = name;
     this.makeEggs = makeEggs.bind(this);
     this.fly = fly.bind(this);
     this.swimm = swimm.bind(this);
     this.eat = eat.bind(this);
     this.hunt = hunt.bind(this);
     this.sing = sing.bind(this);
     this.post = post.bind(this);
     this.run = run.bind(this);
     }

     var bird1 = new Bird('Курица');
 bird1.eat();
