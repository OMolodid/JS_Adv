/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./classwork/task1.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./classwork/eat.js":
/*!**************************!*\
  !*** ./classwork/eat.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst eat = function(){\r\n  // console.log(this.name);\r\n  switch (this.name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Ем травку и козявок\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Люблю кушать семечки.\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Я неприхотлива в еде. Зернышки вполне устроят.\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Люблю кушать рыбу.\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Кушаю рыбку и суши\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"На ужин предпочитаю мышей и всякую мелочь!\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"На ужин предпочитаю мышей и всякую мелочь!\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (eat);\r\n\n\n//# sourceURL=webpack:///./classwork/eat.js?");

/***/ }),

/***/ "./classwork/fly.js":
/*!**************************!*\
  !*** ./classwork/fly.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst fly = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Увы, я не летаюю За то бегаю классно.\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Я могу долго летать без отдыха.\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Я создана для любви, а не для полетов.\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Увы, я не летаюю За то я прикольная птица.\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Я супер-летчик!\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Летаю высоко, гляжу далеко!\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Летаю только по ночам\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (fly);\r\n\n\n//# sourceURL=webpack:///./classwork/fly.js?");

/***/ }),

/***/ "./classwork/hunt.js":
/*!***************************!*\
  !*** ./classwork/hunt.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst hunt = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Я веган. Охота - зло.\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Какая охота?! Дайте семок.\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Я - тварь дрожащая, а не право имеющий. Охотятся только на меня\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Профессионально занимаюсь подводной охотой.\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Охочусь на рыбу\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Охотник-профессионал!\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Супер-классный ночной охотник!\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (hunt);\r\n\n\n//# sourceURL=webpack:///./classwork/hunt.js?");

/***/ }),

/***/ "./classwork/makeEggs.js":
/*!*******************************!*\
  !*** ./classwork/makeEggs.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst makeEggs = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Я страус и я несу огромные яйца\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Голубиные яйца маленькие-маленькие\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Ко-ко-ко! Все знакомы с куриными яйцами\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Я снесла яичко! А высиживать его будет самец. У нас, пингвинов, всегда так\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Мои яйца, вообще, хоть кто-то видел?\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Мои яйца, вообще, хоть кто-то видел?\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Мы, совы, размножаемся почкованием. Шучу.\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (makeEggs);\r\n\n\n//# sourceURL=webpack:///./classwork/makeEggs.js?");

/***/ }),

/***/ "./classwork/post.js":
/*!***************************!*\
  !*** ./classwork/post.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst post = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Почта? Это что, вообще?\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"О да! Крепи скорее письмо к лапке! Я готов.\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"(делает удивленные глаза, не понимает, то от нее хотят)\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Почта? Вы о чем?\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Почта? Вы о чем?\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Да ну вас с вашей почтой!\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Ночная доставка почты - двойной тариф\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (post);\r\n\n\n//# sourceURL=webpack:///./classwork/post.js?");

/***/ }),

/***/ "./classwork/run.js":
/*!**************************!*\
  !*** ./classwork/run.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst run = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Не догониииишь...!!\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Не, я не бегаю. Я лениво хожу. Курлык\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Очень даже бегаю. Метров 5. Потом падаю замертво.\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"Вы лапы мои видели?! Бег? Ага.. щаз.\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Рожденный летать бегать не будет!\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Рожденный летать бегать не будет!\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Рожденный летать бегать не будет!\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (run);\r\n\n\n//# sourceURL=webpack:///./classwork/run.js?");

/***/ }),

/***/ "./classwork/sing.js":
/*!***************************!*\
  !*** ./classwork/sing.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst sing = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Я промолчу.\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Курлык!!\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Куд-кудах-так-тах!\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"...\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Уик!\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Кря! ой..\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"У-у.. У-у!\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (sing);\r\n\n\n//# sourceURL=webpack:///./classwork/sing.js?");

/***/ }),

/***/ "./classwork/swimm.js":
/*!****************************!*\
  !*** ./classwork/swimm.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst swimm = function(name){\r\n  switch (name) {\r\n      case \"Страус\":\r\n          {\r\n              console.log(\"Я-то даже лужу не видел.\");\r\n              break;\r\n          };\r\n      case \"Голубь\":\r\n          {\r\n              console.log(\"Не, плавать не моё.\");\r\n              break;\r\n          };\r\n\r\n      case \"Курица\":\r\n          {\r\n              console.log(\"Иногда плаваю в корыте. Уровень - новичок.\");\r\n              break;\r\n          };\r\n\r\n      case \"Пингвин\":\r\n          {\r\n              console.log(\"ЮЮюююхуууу!!! Поплыли!! Бульк.\");\r\n              break;\r\n          };\r\n      case \"Чайка\":\r\n              {\r\n              console.log(\"Да я жить на воде могу!\");\r\n              break;\r\n              };\r\n      case \"Ястреб\":\r\n              {\r\n              console.log(\"Хммм... Надо попробовать.. Может и поплыву.\");\r\n              break;\r\n              };\r\n      default:\r\n          {\r\n              console.log(\"Нет, спасибо. Я сухопутное создание\");\r\n\r\n          };\r\n        }\r\n}\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (swimm);\r\n\n\n//# sourceURL=webpack:///./classwork/swimm.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _makeEggs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./makeEggs */ \"./classwork/makeEggs.js\");\n/* harmony import */ var _fly__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fly */ \"./classwork/fly.js\");\n/* harmony import */ var _swimm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./swimm */ \"./classwork/swimm.js\");\n/* harmony import */ var _eat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./eat */ \"./classwork/eat.js\");\n/* harmony import */ var _hunt__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hunt */ \"./classwork/hunt.js\");\n/* harmony import */ var _sing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sing */ \"./classwork/sing.js\");\n/* harmony import */ var _post__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./post */ \"./classwork/post.js\");\n/* harmony import */ var _run__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./run */ \"./classwork/run.js\");\n/*\r\n\r\n  Задание:\r\n    Написать конструктор обьекта. Отдельные функции разбить на модули\r\n    и использовать внутри самого конструктора.\r\n    Каждую функцию выделить в отдельный модуль и собрать.\r\n\r\n    Тематика - птицы.\r\n    Птицы могут:\r\n      - Нестись\r\n      - Летать\r\n      - Плавать\r\n      - Кушать\r\n      - Охотиться\r\n      - Петь\r\n      - Переносить почту\r\n      - Бегать\r\n\r\n  Составить птиц (пару на выбор, не обязательно всех):\r\n      Страус\r\n      Голубь\r\n      Курица\r\n      Пингвин\r\n      Чайка\r\n      Ястреб\r\n      Сова\r\n\r\n */\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n function Bird(name) {\r\n     this.name = name;\r\n     this.makeEggs = _makeEggs__WEBPACK_IMPORTED_MODULE_0__[\"default\"].bind(this);\r\n     this.fly = _fly__WEBPACK_IMPORTED_MODULE_1__[\"default\"].bind(this);\r\n     this.swimm = _swimm__WEBPACK_IMPORTED_MODULE_2__[\"default\"].bind(this);\r\n     this.eat = _eat__WEBPACK_IMPORTED_MODULE_3__[\"default\"].bind(this);\r\n     this.hunt = _hunt__WEBPACK_IMPORTED_MODULE_4__[\"default\"].bind(this);\r\n     this.sing = _sing__WEBPACK_IMPORTED_MODULE_5__[\"default\"].bind(this);\r\n     this.post = _post__WEBPACK_IMPORTED_MODULE_6__[\"default\"].bind(this);\r\n     this.run = _run__WEBPACK_IMPORTED_MODULE_7__[\"default\"].bind(this);\r\n     }\r\n\r\n     var bird1 = new Bird('Курица');\r\n bird1.eat();\r\n     //оно, вообще, будет работать? Или нужнобіло все засунуть в 1 файл и\r\n     //написать export { makeEggs, fly, etc.. };??\r\n\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ })

/******/ });