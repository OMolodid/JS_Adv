/*

  localStorage
  window.localStorage

*/
//
// // Запись в ЛС
// // localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'blue');
// // // Чтение с ЛС
// // var cat = localStorage.getItem("myCat");
// // // Удаление с ЛС
// // localStorage.removeItem("myCat");
// // console.log( cat );
// // Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log( window.localStorage );
//
// console.log( JSON.parse( localStorage.getItem("JSON") ) );
//
// if( background !== null){
//   document.body.style.backgroundColor = background;
// }

/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/


var app = document.getElementById('app');
var button = document.createElement('button');
button.innerText = 'Click on me';
button.setAttribute('onclick', 'changeColor()');
app.appendChild(button);


function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function changeColor(){
var random = '#';
for (i=1;i<4;i++){
  random+=getRandomIntInclusive(0, 256).toString(16);
}

document.body.style.background = random;
localStorage.setItem('color', random);
}
var background = localStorage.getItem("color");

if( background !== null){
  document.body.style.backgroundColor = background;
}

const login = document.getElementById('login');
const pass = document.getElementById('pass');
const submit = document.getElementById('submit');
const out = document.getElementById('out');
const hi = document.getElementById('hi');


submit.addEventListener('click', function(e){
  e.preventDefault();

  localStorage.setItem('user', login.value);
  localStorage.setItem('password', pass.value);

    hi.innerHTML = `Hi, ${localStorage.getItem("user")}! Nice to see you again!`
  login.classList.add('hidden');
  pass.classList.add('hidden');
  submit.classList.add('hidden');

  });

  document.addEventListener('DOMContentLoaded', function(){

var user = localStorage.getItem("user");
var password = localStorage.getItem("password");
  if(( user !== null)&&( password !== null)){

    hi.innerHTML = `Hi, ${localStorage.getItem("user")}! Nice to see you again!`
    login.classList.add('hidden');
    pass.classList.add('hidden');
    submit.classList.add('hidden');
  }

});
out.addEventListener('click', function(e){
  e.preventDefault();
  localStorage.removeItem('user');
  localStorage.removeItem('password');
login.classList.remove('hidden');
pass.classList.remove('hidden');
submit.classList.remove('hidden');
hi.innerHTML = '';
  });
