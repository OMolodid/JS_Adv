var buttonContainer = document.getElementById('buttonContainer');
var buttons = Array.from(buttonContainer.getElementsByClassName('showButton'));

for (i=0;i<buttons.length;i++){
  buttons[i].setAttribute('onClick', 'openTab(event)');
}

var tabContainer = document.getElementById('tabContainer');
var tabs = Array.from(tabContainer.getElementsByClassName('tab'));


function openTab(event){
    var num = event.target.dataset.tab;
  for (j=0;j<tabs.length;j++){
  if ( num == tabs[j].dataset.tab){
    tabs[j].classList.add('active');
    break
  }
}
}

var newButton = document.createElement('button');
newButton.className = "closeButton";
newButton.innerText = 'Закрыть всё и сразу';
newButton.setAttribute('onClick', 'closeTabs(event)');
buttonContainer.appendChild(newButton);

function closeTabs(event){

  for (j=0;j<tabs.length;j++){
  if (tabs[j].classList.contains('active')){
    tabs[j].classList.remove('active');
  }
}
}


  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
