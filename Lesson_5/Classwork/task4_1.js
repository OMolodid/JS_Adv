const conf =
[
 {
    first: 97,
    last: 122
  },
   {
    first: 65,
    last: 90
  },
   {
    first: 1072,
    last: 1103
  },
   {
    first: 1040,
    last: 1071
  }
];
var input = document.getElementById('input');
var btnCrypt =  document.getElementById('encryptButton');
var btnDecrypt =  document.getElementById('decryptButton');
var resultBlock = document.getElementById('result');
var keyOption = document.getElementById('symNum');
var CryptedWord =[];
var DeCryptedWord =[];


var input1 = document.getElementById('input1');
var btnCrypt1 =  document.getElementById('encryptButton1');
var btnDecrypt1 =  document.getElementById('decryptButton1');

var input2 = document.getElementById('input2');
var btnCrypt2 =  document.getElementById('encryptButton2');
var btnDecrypt2 =  document.getElementById('decryptButton2');

var input3 = document.getElementById('input3');
var btnCrypt3 =  document.getElementById('encryptButton3');
var btnDecrypt3 =  document.getElementById('decryptButton3');

var input4 = document.getElementById('input4');
var btnCrypt4 =  document.getElementById('encryptButton4');
var btnDecrypt4 =  document.getElementById('decryptButton4');

var input5 = document.getElementById('input5');
var btnCrypt5 =  document.getElementById('encryptButton5');
var btnDecrypt5 =  document.getElementById('decryptButton5');

const crypt = (count, word) => {
  let lastCharCode;
  let firstCharCode;

  for ( i = 0; i < word.length; i++) {
    const currentCharCode = word.charCodeAt(i);
      for(let symb of conf) {
        if (currentCharCode>=symb.first&&currentCharCode<=symb.last){
          firstCharCode = symb.first;
          lastCharCode = symb.last;
          break;
        }
      }
        let newCharCode = word.charCodeAt(i)+count;
        let difShift = newCharCode - lastCharCode;
        if(difShift>0){newCharCode = firstCharCode+difShift - 1;}
        let newChar = String.fromCharCode(newCharCode);
        CryptedWord.push(newChar);
      }
  return CryptedWord;
};

const deCrypt = (count, word) => {
  let lastCharCode;
  let firstCharCode;

  for ( i = 0; i < word.length; i++) {
    const currentCharCode = word.charCodeAt(i);
      for(let symb of conf) {
        if (currentCharCode>=symb.first&&currentCharCode<=symb.last){
          firstCharCode = symb.first;
          lastCharCode = symb.last;
          break;
        }
        }
        let newCharCode = word.charCodeAt(i)-count;
        if (newCharCode<firstCharCode){
          let difShift = newCharCode - firstCharCode;
          newCharCode = lastCharCode+difShift+1;
        }
        let newChar = String.fromCharCode(newCharCode);
        DeCryptedWord.push(newChar);
      }
  return DeCryptedWord;
};

const cryptHandler = (event) => {
  CryptedWord = [];
  DeCryptedWord = [];
  let value = input.value;
  let count = +keyOption.value;
  let result = crypt( count, value );
  let joinedResult = result.join('');
  resultBlock.innerHTML = joinedResult;
};
btnCrypt.addEventListener('click', cryptHandler );

const deCryptHandler = (event) => {
  CryptedWord = [];
  DeCryptedWord = [];
  let value = input.value;
  let count = +keyOption.value;
  let result = deCrypt( count, value );
  let joinedResult = result.join('');
  resultBlock.innerHTML = joinedResult;
};
btnDecrypt.addEventListener('click', deCryptHandler );


const CryptHanlders = () => {
  const inputs = [
    { input: input1, button: btnCrypt1 },
    { input: input2, button: btnCrypt2 },
    { input: input3, button: btnCrypt3 },
    { input: input4, button: btnCrypt4 },
    { input: input5, button: btnCrypt5 }
];
        inputs.map( (obj, index) => {
          const bindedFunc = crypt.bind(null, index+1);
          const Handler = () => {
            CryptedWord = [];
            let value = obj.input.value;
            let result = bindedFunc( value );
            let joinedResult = result.join('');
            resultBlock.innerHTML = joinedResult;
          }
          obj.button.addEventListener('click', Handler );
        })
};
CryptHanlders();

const DeCryptHanlders = () => {
  const inputs = [
    { input: input1, button: btnDecrypt1 },
    { input: input2, button: btnDecrypt2 },
    { input: input3, button: btnDecrypt3 },
    { input: input4, button: btnDecrypt4 },
    { input: input5, button: btnDecrypt5 }
];
        inputs.map( (obj, index) => {
          const bindedFunc = deCrypt.bind(null, index+1);
          const Handler = () => {
            CryptedWord = [];
            DeCryptedWord = [];
            let value = obj.input.value;
            let result = bindedFunc( value );
            let joinedResult = result.join('');
            resultBlock.innerHTML = joinedResult;
          }
          obj.button.addEventListener('click', Handler );
        })
};
DeCryptHanlders();
