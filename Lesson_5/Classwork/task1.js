/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var train = {
  name: 'intercity',
  speed: 350,
  passNum: 500,
  go: function(){
  var newSpeed = +prompt('Set new speed');
  this.speed = newSpeed;
    console.log( 'Поезд '+ this.name+' везет '+ this.passNum+ ' пассажиров.');
  },

stop: function(){
  this.speed = 0;
    console.log( 'Поезд '+ this.name+' остановился. Скорость '+ this.speed+ '.');
  },
  getMorePass: function(x){
    this.passNum += x;
    console.log('New passNum:', this.passNum)
  }
};

train.getMorePass(5);
train.go();
train.stop();
console.log(train);
