/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/
function Dog(name, bread) {
    this.name = name;
    this.surname = bread;
    this.action = function(act) {
      console.log("Собачка " + this.name  + " "+ act);
    }
  };
var tuzik = new Dog ('Тузик','дворняга');
var act = prompt('Что собачка делает?');
tuzik.action(act);
console.log(tuzik);

function showProp(obj){
for (key in obj) {
  console.log( key, obj[key] );
}
}
showProp(tuzik);
