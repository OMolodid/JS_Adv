/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);
}
    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
var commentsFeed = document.getElementById('CommentsFeed');

function Comment(name, text, avatarUrl) {
    if(name!==undefined){
    this.name = name
  } else {
    this.name = protoComment.name};
    if(text!==undefined){
    this.text = text
  } else {
    this.text = protoComment.text};
    if(avatarUrl!==undefined){
    this.avatarUrl = avatarUrl
  } else {
    this.avatarUrl = protoComment.avatarUrl};
    this.likes = 0;
    commentsArray.push(this);
}

var protoComment = {
  name: 'Anonymus',
  text: 'No Comment',
  avatarUrl: 'images/avatar.jpg',
  likesBooster: function(){
    var boost = +prompt('На сколько увеличить лайки?');
    this.likes += boost;
    console.log(this.likes);

  }
}
commentsArray =[];

var myComment1 = new Comment('Kira',"L, Do You know Gods Of Death Love Apples?",'images/ryuk.jpg');
Object.setPrototypeOf( myComment1, protoComment);

var myComment2 = new Comment("L","????",'images/l.jpg');
Object.setPrototypeOf( myComment2, protoComment);

var myComment3 = new Comment();
Object.setPrototypeOf( myComment3, protoComment);

var myComment4 = new Comment();
Object.setPrototypeOf( myComment4, protoComment);

var showMeComments = function(e){

commentsArray.forEach(function(i){

  var pName = document.createElement('p');
  pName.class = 'username';
  var pText = document.createElement('p');
  pText.class = 'usertext';
  var img = new Image('100','100');
  var newLikes = i.likesBooster.bind(i);
  function  boostMyLikes() {
    newLikes();
      likesAmmount.innerHTML = i.likes+'<br>'+'__________________________________'+'<br>';
  }
  img.addEventListener('click', boostMyLikes);
  var likesAmmount = document.createElement('span');
    pName.innerText = i.name;
    commentsFeed.appendChild(pName);
    pText.innerText = i.text;
    commentsFeed.appendChild(pText);
    img.setAttribute('src', i.avatarUrl);
    commentsFeed.appendChild(img);
    likesAmmount.innerHTML = i.likes+'<br>'+'__________________________________'+'<br>';
    commentsFeed.appendChild(likesAmmount);

  });
}
showMeComments(commentsArray);
