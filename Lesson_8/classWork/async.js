/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
async function getCompany(){
  const getCompanyList = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
  const com = await getCompanyList.json();
  const x = [];
    com.forEach(function(i){
      let y ={};
      //
     y.company = i.company,
     y.balance = i.balance,
     y.registered = i.registered,
     y.address = i.address;
     x.push(y);
  });
  console.log(x);
  let table = document.createElement('table');
  for(let y of x){
  let tr = document.createElement('tr');
      tr.innerHTML = `
        <td >
          ${y.company}
        </td>
        <td>
          ${y.balance}
        </td>
        <td>
        <button class = "reg" >Registration date</button>
        </td>
        <td>
        <button class = "ad" >Company address</button>
        </td>
        <td class = "trad">

        </td>
        `
        table.appendChild(tr);
        let regBtn = tr.querySelector('.reg');
        regBtn.addEventListener('click', function(){
          regBtn.innerHTML = y.registered
        });
          let adBtn = tr.querySelector('.ad');
          adBtn.addEventListener('click', function(){
            let trad = tr.querySelector('.trad');
            let {city, zip, country, state, street} = y.address;
            trad.innerHTML = `City: ${city},
             ZIP: ${zip},
             Country: ${country},
             State: ${state},
             Street: ${street}`;
        });
      }
      let target = document.getElementById('target');
      target.appendChild(table);
  
}
getCompany()
